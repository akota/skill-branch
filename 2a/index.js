var express = require('express')
  , cors = require('cors')
  , app = express();

app.use(cors());

app.get('/', function(req, res, next) {
  const sum = (+req.query.a || 0) + (+req.query.b || 0);
  res.send(sum.toString());
});

app.listen(3001, function(){
  console.log('CORS-enabled web server listening on port 3001');
});
