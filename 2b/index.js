var express = require('express')
  , cors = require('cors')
  , app = express();

app.use(cors());

app.get('/', function(req, res, next) {

  var fullname   = req.query.fullname.trim().toLowerCase()
    , nameArr    = fullname.split(/\s+/)
    , surname    = nameArr[nameArr.length - 1]
    , name       = ''
    , patronymic = ''
    , result     = 'Invalid fullname';

  if (surname && !/\d|\W(!.)|_|\//.test(fullname)) {

    surname = surname.charAt(0).toUpperCase() + surname.slice(1);

    if (nameArr.length == 3) {
      name       = nameArr[0].charAt(0).toUpperCase();
      patronymic = nameArr[1].charAt(0).toUpperCase();
      result     = surname + ' ' + name + '. ' + patronymic + '.';
    } else if (nameArr.length == 2) {
      name   = nameArr[0].charAt(0).toUpperCase();
      result = surname + ' ' + name + '.';
    } else if (nameArr.length == 1) {
      result = surname;
    }
  };

  res.send(result);
});

app.listen(3001, function(){
  console.log('CORS-enabled web server listening on port 3001');
});
