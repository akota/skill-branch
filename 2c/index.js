var express = require('express')
  , cors = require('cors')
  , app = express();

app.use(cors());

app.get('/', function(req, res, next) {

  var url     = req.query.username
    , regular = /(https?:)?(\/\/)?((.)[^\/]*\/)?\@?([\w.\_]*)/
    , name    = '@' + url.match(regular)[5];

  res.send(name);
});

app.listen(3001, function(){
  console.log('CORS-enabled web server listening on port 3001');
});
